
public class Test {

	public static void main(String[] args) {
		int[] arr = { 10, 2, 5, 1, 3, 8, 11, -15, 9 };
		Test abc = new Test();
		for (int pt : arr) {
			System.out.print(pt + " ");
		}
		System.out.println();
		abc.quickSort(arr, 0, arr.length - 1);
		for (int pt : arr) {
			System.out.print(pt + " ");
		}
	}

	private int choosePivot(int[] arr, int left, int right) {
		int median = (left + right) / 2;
		int[] arr_1 = { arr[left], arr[median], arr[right] };
		selectionSort(arr_1);
		if (arr_1[1] == arr[left])
			return left;
		else if (arr_1[1] == arr[right])
			return right;
		else
			return median;
	}

	public void selectionSort(int[] arr) {
		int minIndex;
		for (int i = 0; i < arr.length - 1; i++) {
			minIndex = i;
			for (int j = i + 1; j < arr.length; j++) {
				if (arr[j] < arr[minIndex])
					minIndex = j;
			}
			if (minIndex != i)
				displayment(arr, minIndex, i);
		}
	}

	public void quickSort(int[] arr, int left, int right) {
		int index = partition(arr, left, right);
		if (left < index - 1)
			quickSort(arr, left, index - 1);
		if (index < right)
			quickSort(arr, index, right);
	}

	private int partition(int[] arr, int left, int right) {
		int i = left, j = right;
		int pivot = arr[choosePivot(arr, left, right)];
		while (i <= j) {
			while (arr[i] < pivot)
				i++;
			while (arr[j] > pivot)
				j--;
			if (i <= j) {
				displayment(arr, i, j);
				i++;
				j--;
			}
		}
		return i;
	}

	private void displayment(int[] arr, int i, int j) {
		int temp = arr[i];
		arr[i] = arr[j];
		arr[j] = temp;
	}
}
